import React, {FC, ReactNode} from "react";
import "./CarouselItem.scss";

interface Props {
    children: ReactNode
}

export const CarouselItem: FC<Props> = ({children}) => {
    return (
        <div className="photo">
            {children}
        </div>
    )
};
