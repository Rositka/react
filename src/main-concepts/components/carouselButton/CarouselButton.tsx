import React, {FC} from "react";
import { CarouselIcon } from "../../../typeCarousel";
import "./CarouselButton.scss";

interface Props {
    iconCarousel: CarouselIcon
    onClick: (e: React.MouseEvent<HTMLButtonElement>) => void
    disabled?: boolean
}

export const CarouselButton: FC<Props> = ({iconCarousel, onClick, disabled}) => {
    return (
        <button type="button" className="button-icon" onClick={onClick} disabled={disabled}>
            <i className={iconCarousel.icon} />
        </button>
    )
};
