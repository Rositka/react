import React, {FC, ReactNode, useState} from "react";
import {carouselIcon} from "../../../carouselInfo";
import clsx from "clsx";
import "./Carousel.scss";
import {CarouselButton} from "../carouselButton/CarouselButton";

interface Props {
    children: ReactNode[]
}

export const Carousel: FC<Props> = ({children}) => {
    const [activeStep, setActiveStep] = useState(0);

    const onNext = () => {
        setActiveStep(activeStep + 1);
    }

    const onPrevious = () => {
        setActiveStep(activeStep - 1);
    }

    return (
        <div>
            <div className="block-button">
                <CarouselButton iconCarousel={carouselIcon[0]} onClick={onPrevious} disabled={activeStep === 0} />
                <CarouselButton iconCarousel={carouselIcon[1]} onClick={onNext} disabled={activeStep === children.length - 1}/>
            </div>
            {children && children[activeStep]}
            <div className="donuts">
                {
                    new Array(React.Children.count(children))
                        .fill(null).map((_, index) => index)
                        .map((item, key) => {
                            const classess = clsx('step', {
                                step__active: item === activeStep
                            })
                            return <span className={classess} key={item.toString()}/>
                        })
                }
            </div>
        </div>
    )
}
