import React, {FC} from 'react';
import './MainCarousel.scss';
import {Carousel} from '../components/carousel/Carousel';
import {CarouselItem} from '../components/carouselItem/СarouselItem';
import photo1 from '../inserts/images/photo1.jpg';
import photo2 from '../inserts/images/photo2.jpg';
import photo3 from '../inserts/images/photo3.jpg';


export const MainCarousel: FC = () => {
  return (
    <div className="Carousel">
        <Carousel>
            <CarouselItem>
                <img alt="Los Angeles" src={photo1} className="photo__img"/>
                <div className="photo__text">
                    <h2 className="photo__text-header">Los Angeles</h2>
                    <p className="photo__text-para">LA is always so much fun!</p>
                </div>
            </CarouselItem>
            <CarouselItem>
                <img alt="Chicago" src={photo2} className="photo__img" />
                <div className="photo__text">
                    <p className="para">When we arrived at the said place called "Chicagou" which,
                        has taken this name because of the quantity of garlic which
                        grows in the forests in this region.</p>
                    <h2 className="photo__text-header">Chicago</h2>
                    <p className="photo__text-para">Thank you, Chicago!</p>
                </div>
            </CarouselItem>
            <CarouselItem>
                <img alt="New York" src={photo3} className="photo__img" />
                <div className="photo__text">
                    <h2 className="photo__text-header">New York</h2>
                    <p className="photo__text-para">We love the Big Apple!</p>
                </div>
            </CarouselItem>
            <CarouselItem>
                <img alt="Sweden" src='https://techcrunch.com/wp-content/uploads/2016/01/sweden.jpg?w=1390&crop=1' className='photo__img' />
                <div className="photo__text">
                    <h2 className="photo__text-header">Sweden Is A Tech Superstar From The North</h2>
                </div>
            </CarouselItem>
        </Carousel>
    </div>
  );
}

