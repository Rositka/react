import {User} from "./typeUser";
import photo from "./react-overview-components/inserts/images/photo.jpg";

export const userInfo: User =
    {
        contacts: "Contacts",
        photo: photo,
        name: "Rosita Montoini",
        professionMain: "Freelance designer",
        email: "r.montoini@gmail.com",
        phone: "+38 097 6905603",
        gitlab: "https://gitlab.com/Rositka",
        github: "https://github.com/rosita-montoini?tab=repositories",
        experience: [
            {
                company: 'Polygraphy',
                position: 'Designer',
                dateRange: {
                    from: '06/2008',
                    to: '07/2016'
                },
                responsibility: [
                    "Design of printed products",
                    "Development of corporate style, logo",
                    "Customer communication skills",
                    "Creativity, sense of color and style"
                ],
                country: 'Ukraine'
            },
            {
                company: 'Advertising agency Aries',
                position: 'Designer-graphic',
                dateRange: {
                    from: '09/2016',
                    to: '08/2018'
                },
                responsibility: [
                    "Design of printed products",
                    "Development of corporate style, logo",
                    "Customer communication skills",
                    "Creativity, sense of color and style"
                ],
                country: 'Ukraine'
            },
            {
                company: 'Freelance',
                position: 'Designer-graphic',
                dateRange: {
                    from: '07/2018',
                    to: 'up to now'
                },
                responsibility: [
                    "Design of printed products",
                    "Development of corporate style, logo",
                    "Knowledge of graphics programs"
                ]
                ,
                country: 'Ukraine'
            }
        ],
        education: [
            {
                profession: 'Design',
                dateRange: {
                    from: '09/2000',
                    to: '06/2004'
                },
                country: 'Ukraine',
                organization: 'Cherkasy state business college'
            },
            {
                profession: 'Graphic artist',
                dateRange: {
                    from: '09/2004',
                    to: '07/2008'
                },
                country: 'Ukraine',
                organization: 'Cherkasy National University named after Bohdan Khmelnitsky'
            }
        ],
        skills: [
            {
                title: 'Tech Skills',
                skillsList: ['HTML5', 'CSS3', 'JavaScript', 'CorelDRAW', 'Photoshop']
            },
            {
                title: 'Soft Skills',
                skillsList: ['Organization', 'Teamwork', 'Ability', 'Purposefulness']
            }
        ]
    }


