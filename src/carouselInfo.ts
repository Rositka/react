import { CarouselIcon } from "./typeCarousel";
import 'font-awesome/css/font-awesome.min.css';

export const carouselIcon: CarouselIcon[] =
    [
        {
            icon: "fa fa-chevron-left fa-2x"

        },
        {
            icon: "fa fa-chevron-right fa-2x"
        }
    ]
