import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Navigation } from './modules/navigation/navigation';
import { Home } from './modules/home/home';
import { CV } from '../react-overview-components/modules/CV/CV';
import { MainCarousel } from '../main-concepts/modules/MainCarousel';
import { ChooseCountry } from '../context-refs/modules/chooseCountry/chooseCountry';
import { Hooks } from './modules/hooks/hooks';
import '../App.css';

export const App = () => {
    return (
        <Router>
            <Navigation/>
            <Switch>
                <Route path='/home' component={ Home }/>
                <Route path='/about' component={ CV }/>
                <Route path='/carousel' component={ MainCarousel }/>
                <Route path='/holidays' component={ ChooseCountry }/>
                <Route path='/hooks' component={ Hooks }/>
            </Switch>
        </Router>
    );
};
