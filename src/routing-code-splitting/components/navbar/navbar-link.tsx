import React, {FC} from 'react';
import {NavLink} from "react-router-dom";

type Props = {
    url: string,
    caption: string
}

export const NavbarLink: FC<Props> = ({url, caption}) => {
    return (
        <NavLink to={url} className='nav-link'>
            {caption}
        </NavLink>
    )
};
