import React, {FC, ReactNode} from 'react';
import './navbar.scss';

type Props = {
    children: ReactNode,
};

export  const Navbar: FC<Props> = props => {
    return (
        <nav className='navbar'>
            {props.children}
        </nav>
    )
};
