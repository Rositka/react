import React, { FC } from 'react';
import { BrowserRouter as Router, NavLink, Route, Switch, useRouteMatch } from 'react-router-dom';
import { Counter } from '../../../hooks/componets/counter/Counter';
import { Filter } from '../../../hooks/componets/filter/Filter';
import { Nav, NavbarItem } from '../../components/navbar';
import './hooks.scss';
import { NotFound } from '../../../hooks/componets/notFound/notFound';

export const Hooks: FC<{}> = () => {
    let { path, url } = useRouteMatch();

    return (
        <Router>
            <Nav>
                <NavbarItem>
                    <NavLink to={`${url}/use-filter`} className='nav-link-new'>Hook useFilter</NavLink>
                </NavbarItem>
                <NavbarItem>
                    <NavLink to={`${url}/use-logger`} className='nav-link-new'>Hook useLogger</NavLink>
                </NavbarItem>
            </Nav>
            <Switch>
                <Route exact path={`${path}/`} />
                <Route path={`${path}/use-filter`} component={Filter} />
                <Route path={`${path}/use-logger`} component={Counter} />
                <Route  path={`${path}/*`} component={NotFound}/>
            </Switch>
        </Router>
    );
};
