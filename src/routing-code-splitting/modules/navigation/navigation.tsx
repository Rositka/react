import React, {FC} from 'react';
import {Nav, Navbar, NavbarLink, NavbarItem} from '../../components/navbar';

type Props = {};

export const Navigation: FC<Props> = () => {
    return (
        <Navbar>
            <Nav>
                <NavbarItem>
                    <NavbarLink url='/home' caption='HomePage' />
                </NavbarItem>
                <NavbarItem>
                    <NavbarLink url='/about' caption='About' />
                </NavbarItem>
                <NavbarItem>
                    <NavbarLink url='/carousel' caption='Carousel' />
                </NavbarItem>
                <NavbarItem>
                    <NavbarLink url='/holidays' caption='Holidays' />
                </NavbarItem>
                <NavbarItem>
                    <NavbarLink url='/hooks' caption='Hooks' />
                </NavbarItem>
            </Nav>
        </Navbar>
    )
};
