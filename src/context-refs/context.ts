import {createContext} from 'react';
import {Country, Dictionary} from './typedef';

export type Context = {
    country: Country,
    dictionary: Dictionary,
    lang: string,
    countryCode: string,
    chosenCountry: boolean,
    changeLang: (lang: string) => void,
    changeCountryCode: (countryCode: string) => void,
    changeChosenCountry: (state: boolean) => void
}

export const MyContext = createContext<Context | null>(null)
