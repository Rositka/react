import React, {FC, useContext} from 'react';
import {Context, MyContext} from "../../context";
import './selectLang.scss';

export const SelectLang: FC<{}> = () => {
    const context = useContext<Context | null>(MyContext);

    if (!context) {
        return null;
    }

    const handleChangeLang = (e: React.ChangeEvent<HTMLSelectElement>) => {
        context?.changeLang(e.target.value)
    }

    return (
        <>
            {context?.changeChosenCountry && <select className='dropdown' onChange={handleChangeLang}>
                <option value="EN">EN</option>
                <option value={context?.countryCode}>{context?.countryCode}</option>
            </select>}
        </>
    )
}
