import React, {FC, useContext} from 'react';
import {Context, MyContext} from "../../context";
import './selectHoliday.scss';

export const ShowHoliday: FC<{}> = () => {
    const context = useContext<Context | null>(MyContext);

    if (!context) {
        return null;
    }

    return (
        <div className='wrapper-holiday'>
            <div className='block-holiday'>
                {context?.changeChosenCountry &&
                    (context.lang === "EN") ?
                        context.dictionary.map((item, index) => (
                            <p key={index}>{item.date} {item.name}</p>
                        ))
                        :
                        context.dictionary.map((item, index) => (
                            <p key={index}>{item.date} {item.localName}</p>
                        ))
                }
            </div>
        </div>
    )
}
