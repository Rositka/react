import React, {FC, useContext} from 'react';
import {Context, MyContext} from "../../context";
import './selectCountry.scss';

export const SelectCountry: FC<{}> = () => {
    const context = useContext<Context | null>(MyContext);

    if (!context) {
        return null;
    }

    const handleChangeCountry = (e: React.ChangeEvent<HTMLSelectElement>) => {
        if (e.target.value !== '') {
            context?.changeCountryCode(e.target.value);
            context?.changeChosenCountry(true);
        } else {
            context?.changeChosenCountry(false);
        }
    }

    return (
        <div className="block">
            <p className="block-para">An overview of all supported countries</p>
            <select className="block-select" onChange={handleChangeCountry}>
                <option value=''>Select country</option>
                {context?.country.map((item, index) =>
                    <option key={index} value={item.key}>{item.value}</option>
                )}
            </select>
        </div>
    )
}
