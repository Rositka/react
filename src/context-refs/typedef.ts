export type Country = [
    {
        key: string,
        value: string
    }
]

export type Dictionary = [
    {
        date: string,
        localName: string,
        name: string
    }
]
