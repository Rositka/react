import React, {FC, useEffect, useState} from 'react';
import {MyContext} from '../../context';
import {Country, Dictionary} from '../../typedef';
import {SelectCountry} from '../../components/selectCountry/selectCountry';
import {SelectLang} from "../../components/selectLang/selectLang";
import {ShowHoliday} from "../../components/showHoliday/showHoliday";
import './chooseCountry.scss';
import '../../../App.css';

export const ChooseCountry: FC<{}> = () => {
    const [lang, setLang] = useState<string>('EN');
    const [country, setCountry] = useState<Country[]>([]);
    const [countryCode, setCountryCode] = useState<string>('');
    const [dictionary, setDictionary] = useState<Dictionary[]>([]);
    const [chosenCountry, setChosenCountry] = useState<boolean>(false);

    useEffect(() => {
        fetch('https://gcp-test-yq2tp6xfda-ew.a.run.app/Api/v2/AvailableCountries')
            .then(result => result.json())
            .then(data => {
                setCountry(data);
            });
    }, []);

    useEffect(() => {
        if (!countryCode) {
            return
        }
        fetch(`https://gcp-test-yq2tp6xfda-ew.a.run.app/Api/v2/PublicHolidays/2021/${countryCode}`)
            .then(result => result.json())
            .then(data1 => {
                setDictionary(data1);
            });
    }, [countryCode]);

    const contextValue = {
        country,
        dictionary,
        lang,
        countryCode,
        chosenCountry,
        changeLang: (lang: string) => setLang(lang),
        changeCountryCode: (countryCode: string) => setCountryCode(countryCode),
        changeChosenCountry: (state: boolean) => setChosenCountry(state)
    }

    return (
        // @ts-ignore
        <MyContext.Provider value={contextValue}>
            <div className='wrapper'>
                <header className='wrapper-block'>
                    <div className='block'>
                        <SelectCountry />
                        <SelectLang />
                    </div>
                </header>
                <ShowHoliday />
            </div>
        </MyContext.Provider>
    )
}
