export type User = {
    contacts: string,
    photo: string,
    name: string,
    professionMain: string,
    email: string,
    phone: string,
    gitlab: string,
    github: string,
    experience: Experience[],
    education: Education[],
    skills: Skills[]
}


export type Experience = {
    company: string,
    position: string,
    dateRange: DateRange,
    responsibility: string[],
    country: string
}

export type Education = {
    profession: string,
    dateRange: DateRange,
    country: string,
    organization: string
}

export type Skills = {
    title: string,
    skillsList: string[]
}


type DateRange = {
    from: string,
    to: string
}
