import React, {FC} from "react";
import { userInfo } from "../../../../userInfo";
import { ContactsList } from "../сontactsList/ContactsList";
import {User} from "../../../../typeUser";
import "./Contacts.scss";

interface Props {
    user: User
}

export const Contacts:  FC<Props> = ({user}) => {
  return (
      <div className="contacts">
          <div className="contacts__photo">
              <img alt="myPhoto" className="contacts__profile-img" src={user.photo} />
          </div>
          <ContactsList user={userInfo} />
      </div>
  )
};
