import React, { FC } from "react";
import "./Projects.scss";
import {User} from "../../../../typeUser";

interface Props {
    user: User
}

export const Projects: FC<Props> = ({user}) => {
    return (
        <div className="main__projects">
            <h4 className="main__projects-title">Projects</h4>
            <ul className="main__projects-list">
                <li>
                    <a href={user.gitlab}  rel="noreferrer" target="_blank">{user.gitlab}</a>
                </li>
                <li>
                    <a href={user.gitlab}  rel="noreferrer" target="_blank">{user.github}</a>
                </li>
            </ul>
        </div>
    )
}
