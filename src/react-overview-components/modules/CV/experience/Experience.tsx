import React, { FC } from "react";
import {ExperienceItem} from "./experienceItem/ExperienceItem";
import {User} from "../../../../typeUser";

interface Props {
    user: User
}

export const Experience: FC<Props> = ({user}) => {
    return (
        <div>
            <h4 className="main__projects-title">Work Experience</h4>
            {user.experience.map((experienceItem, idx) =>
                <ExperienceItem key={idx} experience={experienceItem} />
            )}
        </div>
    )
}
