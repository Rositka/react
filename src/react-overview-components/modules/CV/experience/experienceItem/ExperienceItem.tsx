import React, { FC } from "react";
import {Experience} from "../../../../../typeUser";
import "./ExperienceItem.scss";

interface Props {
    experience: Experience
}

export const ExperienceItem: FC<Props> = ({experience}) => {
    return (
        <div className="profession">
            <p className="profession__title">{experience.position}
                <span>{experience.company}</span>
            </p>
            <div className="profession__time">
                {experience.dateRange.from} - {experience.dateRange.to}
                <span className="profession__divider"> |</span> {experience.country}
            </div>
            <ul className="profession__duties">
                {experience.responsibility.map((item, index) => {
                    return (
                        <li key={index} className="profession__duties-item">{item}</li>
                    )
                })}
            </ul>
        </div>
    )
}
