import React, { FC } from "react";
import {Education} from "../../../../../typeUser";
import "./EducationItem.scss";

interface Props {
    education: Education
}

export const EducationItem: FC<Props> = ({education} ) => (
    <div className="education">
        <p className="education__inst">{education.organization}</p>
        <p className="education__title">{education.profession}</p>
        <div className="education__time">{education.dateRange.from} - {education.dateRange.to}
            <span className="education__divider"> |</span> {education.country}
        </div>
    </div>
)
