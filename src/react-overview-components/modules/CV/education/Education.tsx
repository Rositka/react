import React, { FC } from "react";
import {EducationItem} from "./educatioItem/EducationItem";
import {User} from "../../../../typeUser";

interface Props {
    user: User
}

export const Education: FC<Props> = ({user}) => {
    return (
        <div>
            <h4 className="main__projects-title">Education</h4>
            {user.education.map((educationItem, idx) =>
                <EducationItem key={idx} education={educationItem} />
            )}
        </div>
    )
}
