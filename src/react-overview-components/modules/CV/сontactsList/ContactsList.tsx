import React, { FC } from "react";
import { User } from "../../../../typeUser";
import "./ContactsList.scss";

interface Props {
    user: User
}

export const ContactsList: FC<Props> = ({user}) => {
    return (
        <ul className="contacts-list">
            <p className="contacts-list__title">{user.contacts}</p>
            <li className="contacts-item">
                <span className="contacts-item__i">C:</span>
                <a href="tel:380976905603" className="contacts-item__tel">{user.phone}</a>
            </li>
            <li className="contacts-item">
                <span className="contacts-item__i">E:</span>
                <a href="mailto:r.montoini@gmail.com" className="contacts-item__email">{user.email}</a>
            </li>
        </ul>
    )
};
