import React from "react";
import {userInfo} from "../../../userInfo";
import {Aside} from "../../components/aside/Aside";
import {Contacts} from "./contacts/Contacts";
import {Skills} from "./skills/Skills";
import {Main} from "../../components/main/Main";
import {MainInfo} from "./mainInfo/MainInfo";
import {Projects} from "./projects/Projects";
import {Experience} from "./experience/Experience";
import {Education} from "./education/Education";
import "./App.scss";

export const CV = () => (
    <div className="App">
        <Aside>
            <Contacts user={userInfo} />
            <Skills user={userInfo} />
        </Aside>
        <Main>
            <MainInfo user={userInfo} />
            <Projects user={userInfo} />
            <Experience user={userInfo} />
            <Education user={userInfo} />
        </Main>
    </div>
)
