import React, { FC } from "react";
import {SkillsItem} from "./skillsItem/SkillsItem";
import {User} from "../../../../typeUser";

interface Props {
    user: User
}

export const Skills: FC<Props> = ({user}) => {
    return (
        <div>
            {user.skills.map((skillsItem, idx) =>
                <SkillsItem key={idx} skills={skillsItem} />
            )}
        </div>
    )
}
