import React, {FC} from "react";
import {Skills} from "../../../../../typeUser";
import "./SkillsItem.scss";

interface Props {
    skills: Skills
}

export const SkillsItem: FC<Props> = ({skills}) => {
    return (
        <div className="skills-container">
            <h3 className="skills-container__title">{skills.title}</h3>
            <ul className="skills-container__list">
                {skills.skillsList.map((item, index) => {
                    return (
                        <li key={index} className="skills-container__item">{item}</li>
                    )
                })}
            </ul>
        </div>
    )
}
