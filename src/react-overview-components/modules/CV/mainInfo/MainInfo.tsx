import React, { FC } from "react";
import {User} from "../../../../typeUser";
import "./MainInfo.scss";

interface Props {
    user: User
}

export const MainInfo: FC<Props> = ({user}) => {
    return (
        <div className="main__info">
            <h2 className="main__info-profession">{user.professionMain}</h2>
            <h1 className="main__info-name">{user.name}</h1>
            <p className="main__info-description">
                <span>Desired position</span>
                <span>Frontend developer</span>
            </p>
        </div>
    )
}




