import React from "react";
import "./Main.scss";

export const Main: React.FC = ({children}) => {
    return (
        <main className="main">
            {children}
        </main>
    )
};
