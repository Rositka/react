import React from "react";
import "./Aside.scss";

export const Aside: React.FC = ({children}) => {
    return (
        <aside className="sidebar">
            {children}
        </aside>
    )
};
