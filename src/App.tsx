import React, {createContext, useContext} from 'react';
import {store} from './project/store/store';
import {Provider} from 'react-redux';
import {BrowserRouter as Router, Redirect, Route, Switch, useRouteMatch} from 'react-router-dom';
import {Navigation} from './project/modules/navigation/navigation';
import {HomePage} from './project/modules/home-page/homePage';
import {Auth} from './project/modules/authentification/auth';
import {SearchReposOrganisation} from './project/modules/search/searchReposOrganisation';
import {ReposDetails} from './project/modules/repos-details/repos-details';
import {OrganisationsDetails} from './project/modules/organization-detais/organisations-details';
import {IssuesList} from './project/modules/issues/issues-list';
import {ContributorsList} from './project/modules/contributors/contributors-list';
import {ForksList} from './project/modules/forks/forks-list';
import {IssueDetails} from "./project/modules/issues/issue-details";

export default function App() {
    let {path} = useRouteMatch();

    return (
        <Provider store={store}>
            <Router>
                <Navigation/>
                <Switch>
                    <Route path={`${path}home`} component={HomePage} exact />
                    <Route path={`${path}auth`} component={Auth} exact />
                    <Route path={`${path}project`} component={SearchReposOrganisation} exact />
                    <Route path={`${path}project/:owner/:repo`} component={ReposDetails} exact />
                    <Route path={`${path}project/:owner/:repo/issues`} component={IssuesList} exact/>
                    <Route path={`${path}project/:owner/:repo/issues/:id`} component={IssueDetails} exact/>
                    <Route path={`${path}project/:owner/:repo/contributors`} component={ContributorsList} exact/>
                    <Route path={`${path}project/:owner/:repo/forks`} component={ForksList} exact/>
                    <Route path={`${path}project/:owner`} component={OrganisationsDetails} exact />
                    <Redirect to={`${path}home`} />
                </Switch>
            </Router>
        </Provider>
    )
};
