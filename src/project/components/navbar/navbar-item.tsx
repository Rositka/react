import React, {FC, ReactNode} from 'react';

type Props = {
    children: ReactNode
};

export const NavbarItem: FC<Props> = ({children}) => {
    return (
        <li className='navbar-item'>
            {children}
        </li>
    )
};
