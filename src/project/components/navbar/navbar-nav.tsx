import React, {FC, ReactNode} from 'react';
import './navbar.scss';

type Props = {
    children: ReactNode
};

export const Nav: FC<Props> = props => {
    return (
        <ul className='navbar-nav'>
            {props.children}
        </ul>
    )
};
