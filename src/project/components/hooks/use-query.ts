import {useHistory, useLocation} from 'react-router-dom';

export function useQuery() {
    const history = useHistory();
    const query = new URLSearchParams(useLocation().search);

    const setQuery = (name: string, value: string) => {
        query.set(name, value)
        history.replace({search: query.toString()});
    };

    return {
        query,
        setQuery
    };
}
