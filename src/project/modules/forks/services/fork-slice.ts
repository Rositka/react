import {createAsyncThunk, createEntityAdapter, createSlice} from '@reduxjs/toolkit';
import {octokit} from '../../../octokit';
import {AppState} from '../../../store/typedef';
import {OrganisationsSearchResult} from '../../typedef';

interface forksParams {
    owner: string,
    repo: string,
}

const forksAdapter = createEntityAdapter<OrganisationsSearchResult>();

export const loadForks = createAsyncThunk('forks', async ({owner, repo}: forksParams) => {
    return (await octokit().repos.listForks({owner, repo})).data
});

export const forks = createSlice({
    name: 'forks',
    initialState: forksAdapter.getInitialState({
        loading: false,
    }),
    reducers: {},
    extraReducers: builder => builder
        .addCase(loadForks.fulfilled, (state, action) => {
            forksAdapter.setAll(state, action);
            state.loading = false;
        })
        .addCase(loadForks.pending, state => {
            state.loading = true;
        })
        .addCase(loadForks.rejected, state => {
            state.loading = false;
        })
});

export const forksSelectors = forksAdapter.getSelectors<AppState>(state => state.forksAll);
