import React, {FC, useCallback} from 'react';
import {useHistory, useRouteMatch} from 'react-router-dom';
import {Box, CardHeader, ListItem, ListItemText} from '@material-ui/core';
import {OrganisationsSearchResult} from '../typedef';

interface Props {
    fork: OrganisationsSearchResult,
}

export const ForksPreview: FC<Props> = ({fork}) => {
    const {url} = useRouteMatch();
    const history = useHistory();

    const openForks = useCallback(() => {
        history.replace(`${url}/${fork.name}`);
    }, [])

    return (
        <>
            <ListItem button={true} onClick={openForks}>
                <CardHeader
                    title={fork.name}
                    subheader={fork.created_at}
                />
                <ListItemText>
                    <Box>Full-name: {fork?.full_name}</Box>
                    <Box>License: {fork?.license?.name}</Box>
                    <Box>Issues: {fork?.open_issues_count}</Box>
                </ListItemText>
            </ListItem>
        </>
    );
};
