import React, {useEffect} from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import {useParams} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import {OwnerRepo} from '../typedef';
import {List} from '@material-ui/core';
import {forksSelectors, loadForks} from './services/fork-slice';
import {ForksPreview} from './forks-preview';

const useStyles = makeStyles((theme) => ({
    paper: {
        maxWidth: '500px',
        width: '100%',
        margin: '0 auto',
    },
}));

export const ForksList = () => {
    const params = useParams<OwnerRepo>();
    const styles = useStyles();
    const dispatch = useDispatch();
    const forks  = useSelector(forksSelectors.selectAll);

    useEffect(() => {
        dispatch(loadForks(params));
    }, []);

    return (
        <div className={styles.paper}>
            <List>
                {
                    forks.map((fork) => {
                        return <ForksPreview fork={fork} key={fork.id}/>;
                    })}
            </List>
        </div>
    )
};
