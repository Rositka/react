import React, {FC} from 'react';
import './homePage.scss';
import logo from '../../inserts/logo2.png';

export const HomePage: FC<{}> = () => {
    return (
        <div>
            <h1 className='block'>Welcome!</h1>
            <img className='logo' src={logo} alt='logo'/>
        </div>
    )
};
