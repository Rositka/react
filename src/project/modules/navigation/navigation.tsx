import React, {FC} from 'react';
import {Nav, Navbar, NavbarItem, NavbarLink} from '../../components/navbar';

export const Navigation: FC<{}> = () => {

    return (
        <Navbar>
            <Nav>
                <NavbarItem>
                    <NavbarLink url='/home' caption='Home'/>
                </NavbarItem>
                <NavbarItem>
                    <NavbarLink url='/auth' caption='Authentication'/>
                </NavbarItem>
                <NavbarItem>
                    <NavbarLink url='/project' caption='Project'/>
                </NavbarItem>
            </Nav>
        </Navbar>
    )
};
