import {components} from "@octokit/openapi-types/generated/types";

export type Issue = components['schemas']['issue'];
export type IssueComment = components['schemas']['issue-comment'];
export type Contributor = components['schemas']['contributor'];
export type Repository = components['schemas']['full-repository'];
export type ReposSearchResult = components['schemas']['repo-search-result-item'];
export type Organisations = components['schemas']['organization-full'];
export type OrganisationsSearchResult = components['schemas']['minimal-repository'];
export type User = components['schemas']['simple-user'];

export type OwnerRepo = {
    owner: string,
    repo: string,
    assignee?: string,
}

export type OwnerOrg = {
    owner: string,
}

