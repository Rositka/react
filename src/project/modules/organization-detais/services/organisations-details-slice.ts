import {createAsyncThunk, createEntityAdapter, createSlice} from '@reduxjs/toolkit';
import {Organisations} from '../../typedef';
import {octokit} from '../../../octokit';
import {AppState} from '../../../store/typedef';

const organisationsDetailsAdapter = createEntityAdapter<Organisations>();

export const loadOrganisationsInfo = createAsyncThunk('organisations-info', async (org: string) => {
    return (await octokit().orgs.get({org})).data;
});

export const organisationsDetails = createSlice({
    name: 'organisations-info',
    initialState: organisationsDetailsAdapter.getInitialState(),
    reducers: {},
    extraReducers: builder => builder
        .addCase(loadOrganisationsInfo.fulfilled, organisationsDetailsAdapter.addOne)
});

export const organisationsInfoSelectors = organisationsDetailsAdapter.getSelectors<AppState>(state => state.organisationsDetails);
