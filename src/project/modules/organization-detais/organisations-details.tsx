import React, {useEffect} from 'react';
import {Box, Card, CardContent, CardHeader, Typography} from '@material-ui/core';
import {useDispatch, useSelector} from 'react-redux';
import {useParams} from 'react-router-dom';
import {makeStyles} from '@material-ui/core/styles';
import {OwnerOrg} from '../typedef';
import {loadOrganisationsInfo, organisationsInfoSelectors} from './services/organisations-details-slice';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import {searchOrgSelectors} from '../search/services/organisations-search-slice';
import {OrgsItem} from './org-item';

const useStyles = makeStyles((theme) => ({
    paper: {
        maxWidth: '500px',
        width: '100%',
        margin: '0 auto',
    },
    avatar: {
        marginLeft: '13px',
        marginTop: '10px'
    }
}));

export const OrganisationsDetails = () => {
    const styles = useStyles();
    const params = useParams<OwnerOrg>();
    const dispatch = useDispatch();
    const organisationsInfo = useSelector(state => state.organisationsDetails.entities[`${state.organisationsDetails.ids[0]}`]);
    const organisationsRepos = useSelector(organisationsInfoSelectors.selectAll);
    const allOrgRepos = useSelector(searchOrgSelectors.selectAll);

    useEffect(() => {
        dispatch(loadOrganisationsInfo(params.owner));
    }, []);

    return (
        <Card className={styles.paper}>
            <Avatar className={styles.avatar} src={`${organisationsInfo?.avatar_url}`}/>
            <CardHeader
                title={organisationsInfo?.login}
                subheader={organisationsInfo?.created_at}
            />
            <CardContent>
                {organisationsInfo?.description}
                <Box>Followers: {organisationsInfo?.followers}</Box>
                <Box>Public repos: {organisationsInfo?.public_repos}</Box>
                <Box>Private repos: {organisationsInfo?.total_private_repos}</Box>
                {
                    !(!organisationsRepos.length) ?
                        <List dense={true}>
                            <Typography align="left">All repositories</Typography>
                            {allOrgRepos.map(org => <OrgsItem org={org} key={org.id}/>)}
                        </List>
                        :
                        <Typography align="left">No repositories</Typography>
                }
            </CardContent>
        </Card>
    )
}
