import React, {useEffect, useState} from 'react';
import {Avatar, Button, makeStyles, TextField, Typography} from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import {useDispatch, useSelector} from 'react-redux';
import {loadToken} from './services/token/action';
import {loadUser} from './services/user/action';

const useStyles = makeStyles(theme => ({
    paper: {
        margin: '0 auto',
        maxWidth: '600px',
        marginTop: '100px',
        width: '100%',
        backgroundColor: theme.palette.background.paper,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(2),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        maxWidth: '200px',
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        margin: '0 auto',
        marginTop: '30px',
    },
    block: {
        marginBottom: '30px',
    }
}));

export const Auth = () => {
    const styles = useStyles();
    const dispatch = useDispatch();
    const [inputValue, setInputValue] = useState('');
    const [visible, setVisible] = useState(true);
    const user = useSelector(state => state.getUser.user);
    const token = useSelector(state => state.getToken.token);

    useEffect(() => {
        localStorage.token = token;
        dispatch(loadToken(localStorage.token));
        dispatch(loadUser);
    }, [token]);

    const getToken = () => {
        localStorage.token = inputValue;
        dispatch(loadToken(localStorage.token));
        dispatch(loadUser);
    };

    const deleteToken = () => {
        localStorage.token = '';
        setInputValue('');
        dispatch(loadToken(''));
        setVisible(visible => !visible);
    }

    return (
        <div className={styles.paper}>
            {!token
                ? <>
                    <Avatar className={styles.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component='h1' variant='h5'>Log in</Typography>
                    <TextField
                        variant='outlined'
                        margin='normal'
                        required fullWidth
                        id='token-input'
                        label='Authentication token'
                        type='password'
                        autoComplete='Authentication token'
                        autoFocus
                        onChange={(e) => setInputValue(e.target.value)}
                    />
                    <Button variant='contained' color='primary' className={styles.submit} onClick={getToken}>
                        Log in
                    </Button>
                </>
                :
                <>
                    <Avatar className={styles.avatar} src={user?.avatar_url}/>
                    <div className={styles.block}>{user?.login}</div>
                    <Typography component='h1' variant='h5'>Log out</Typography>
                    <Button variant='contained' color='primary' className={styles.submit} onClick={deleteToken}>
                        Log out
                    </Button>
                </>
            }
        </div>
    )
};
