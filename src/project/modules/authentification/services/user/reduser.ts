import {createReducer} from '@reduxjs/toolkit';
import {userSuccess} from './action';
import {User} from '../../../typedef';

type UserState = {
    user: User | null,
}

const defaultState = {
    user: null
}

export const getUser = createReducer<UserState>(defaultState, builder =>
   builder
       .addCase(userSuccess, (state, action) => ({
           ...state,
           user: action.payload
       }))
       .addDefaultCase(state => state)
);
