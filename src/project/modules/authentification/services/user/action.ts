import {createAction} from '@reduxjs/toolkit';
import {AppDispatch} from '../../../../store/typedef';
import {User} from '../../../typedef';
import {octokit} from '../../../../octokit';

export const userRequest = createAction('user/request');
export const userSuccess = createAction<User | null>('user/success');
export const userReject = createAction('user/reject');

export const loadUser = async (dispatch: AppDispatch) => {
    dispatch(userRequest);

    try {
        const response = (await octokit().request('/user'));
        if (response.status === 200) {
            dispatch(userSuccess(response.data));
        }
    }
    catch (error) {
        dispatch(userReject());
    }
};
