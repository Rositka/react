import {createAction} from '@reduxjs/toolkit';
import {AppDispatch} from '../../../../store/typedef';

export const tokenSuccess = createAction<string>('token');

export const loadToken = (token: string) => (dispatch: AppDispatch) => {
    dispatch(tokenSuccess(token));
};
