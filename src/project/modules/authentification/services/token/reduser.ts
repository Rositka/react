import {createReducer} from '@reduxjs/toolkit';
import {tokenSuccess} from './action';

type TokenState = {
    token: string,
}

const defaultState = {
    token: ''
}

export const getToken = createReducer<TokenState>(defaultState, builder =>
   builder
       .addCase(tokenSuccess, (state, action) => ({
           ...state,
           token: action.payload
       }))
       .addDefaultCase(state => state)
);
