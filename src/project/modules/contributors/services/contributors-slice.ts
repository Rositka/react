import {createAsyncThunk, createEntityAdapter, createSlice} from '@reduxjs/toolkit';
import {octokit} from '../../../octokit';
import {AppState} from '../../../store/typedef';
import {Contributor} from '../../typedef';

interface contributorsParams {
    owner: string,
    repo: string,
}

const contributorsAdapter = createEntityAdapter<Contributor>();

export const loadContributors = createAsyncThunk('contributors', async ({owner, repo}: contributorsParams) => {
    return (await octokit().repos.listContributors({owner, repo})).data
});

export const contributors = createSlice({
    name: 'contributors',
    initialState: contributorsAdapter.getInitialState({
        loading: false,
    }),
    reducers: {},
    extraReducers: builder => builder
        .addCase(loadContributors.fulfilled, (state, action) => {
            contributorsAdapter.setAll(state, action);
            state.loading = false;
        })
        .addCase(loadContributors.pending, state => {
            state.loading = true;
        })
        .addCase(loadContributors.rejected, state => {
            state.loading = false;
        })
});

export const contributorsSelectors = contributorsAdapter.getSelectors<AppState>(state => state.contributorsAll);
