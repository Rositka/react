import React, {useEffect} from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import {useParams} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import {OwnerRepo} from '../typedef';
import {List, Typography} from '@material-ui/core';
import {contributorsSelectors, loadContributors} from './services/contributors-slice';
import {ContributorsPreview} from './contributors-prewiew';

const useStyles = makeStyles((theme) => ({
    paper: {
        maxWidth: '500px',
        width: '100%',
        margin: '0 auto',
    },
}));

export const ContributorsList = () => {
    const params = useParams<OwnerRepo>();
    const styles = useStyles();
    const dispatch = useDispatch();
    const contributors  = useSelector(contributorsSelectors.selectAll);

    useEffect(() => {
        dispatch(loadContributors(params));
    }, []);

    return (
        <div>
            {
                (!contributors.length) &&
                <Typography align="center">No contributors</Typography>
            }
            <div className={styles.paper}>
                <List>
                    {
                        contributors.map((cont) => {
                            return <ContributorsPreview cont={cont} key={cont.id}/>;
                        })
                    }
                </List>
            </div>
        </div>
    )
};
