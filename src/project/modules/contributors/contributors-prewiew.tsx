import React, {FC, useCallback} from 'react';
import {useHistory, useRouteMatch} from 'react-router-dom';
import {Avatar, Box, ListItem, ListItemAvatar, ListItemText} from '@material-ui/core';
import {Contributor} from '../typedef';

interface Props {
    cont: Contributor,
}

export const ContributorsPreview: FC<Props> = ({cont}) => {
    const {url} = useRouteMatch();
    const history = useHistory();

    const openContributors = useCallback(() => {
        history.replace(`${url}/${cont.name}`);
    }, [])

    return (
        <>
            <ListItem button={true} onClick={openContributors}>
                <ListItemAvatar>
                    <Avatar src={cont.avatar_url}/>
                </ListItemAvatar>
                <ListItemText>
                    <Box>{cont.name}</Box>
                    <Box>{cont.login}</Box>
                    <Box>Organisation: {cont.organizations_url}</Box>
                </ListItemText>
            </ListItem>
        </>
    );
};
