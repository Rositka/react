import React, {useCallback, useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {useQuery} from '../../components/hooks/use-query';
import {useDispatch, useSelector} from 'react-redux';
import SearchBar from 'material-ui-search-bar';
import {List, Typography} from '@material-ui/core';
import {loadRepos, searchReposSelectors, clearData} from './services/repos-seach-slice';
import {loadOrganisations, searchOrgSelectors, clearDataOrg} from './services/organisations-search-slice';
import {Loader} from '../../components/loader/loader';
import {ReposItem} from './repos-item';
import {OrganisationsItem} from './organisations-item';

const useStyles = makeStyles((theme) => ({
   paper: {
       maxWidth: '900px',
       width: '100%',
       margin: '0 auto',
       backgroundColor: theme.palette.background.paper,
    },
    block: {
        maxWidth: '900px',
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between'
    },
    listItem: {
        maxWidth: '400px',
        width: '100%',
    }
}));

export const SearchReposOrganisation = () => {
    const styles = useStyles();
    const browserQuery = useQuery();
    const [query, setQuery] = useState(browserQuery.query.get('search') || '');
    const dispatch = useDispatch();
    const allRepos = useSelector(searchReposSelectors.selectAll);
    const allOrganisations = useSelector(searchOrgSelectors.selectAll);
    const loadingRepos = useSelector(state => state.reposSearch.loading);
    const loadingOrganisation = useSelector(state => state.orgSearch.loading);
    const token = localStorage.token;

    const loadData = () => {
        dispatch(loadRepos(query));
        dispatch(loadOrganisations(query));
    };

    useEffect(() => {
        loadData()
        dispatch(clearData());
        dispatch(clearDataOrg());
    }, [token]);

    const onQueryChange = useCallback((value: string) => {
        setQuery(value);
    }, []);

    const search = () => {
        loadData();
        browserQuery.setQuery('search', query);
    };

    const clearQuery = () => {
        setQuery('');
        loadData();
    };

    return (
        <div className={styles.paper}>
            <SearchBar
                value={query}
                onChange={onQueryChange}
                onCancelSearch={clearQuery}
                onRequestSearch={search}
            />
            {loadingRepos && <Loader/>}
            {loadingOrganisation && <Loader/>}
            {
                (!allRepos.length && !loadingRepos && !allOrganisations.length && !loadingOrganisation) &&
                <Typography align="center">No repos or organisations to display</Typography>
            }
            <div className={styles.block}>
                {
                    !(!allRepos.length && !loadingRepos) &&
                    <List className={styles.listItem} dense={true}>
                        <Typography align="center">All repositories</Typography>
                        {allRepos.map(repo => <ReposItem repo={repo} key={repo.id}/>)}
                    </List>
                }
                {
                    !(!allOrganisations.length && !loadingOrganisation) &&
                    <List className={styles.listItem} dense={true}>
                        <Typography align="center">All organisations</Typography>
                        {allOrganisations.map(org => <OrganisationsItem org={org} key={org.id}/>)}
                    </List>
                }
            </div>
        </div>
    )
};
