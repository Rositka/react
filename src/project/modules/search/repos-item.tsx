import React, {FC} from 'react';
import {useHistory, useRouteMatch} from 'react-router-dom';
import {Avatar, ListItem, ListItemAvatar, ListItemText} from '@material-ui/core';
import {ReposSearchResult} from '../typedef';
import {makeStyles} from '@material-ui/core/styles';

interface Props {
    repo: ReposSearchResult
}

const useStyles = makeStyles((theme) => ({
    paper: {
        maxWidth: '400px',
        width: '100%',
        margin: '0 auto',
    }
}));

export const ReposItem: FC<Props> = ({repo}) => {
    const styles = useStyles();
    let {path} = useRouteMatch();
    let history = useHistory();

    const openRepo = () => {
        history.replace(`${path}/${repo.owner?.login}/${repo.name}`);
    }

    return (
        <ListItem className={styles.paper} button={true} onClick={openRepo}>
            <ListItemAvatar>
                <Avatar src={repo.owner?.avatar_url}/>
            </ListItemAvatar>
            <ListItemText>
                <div>{repo.full_name}</div>
                <div>{repo.description}</div>
            </ListItemText>
        </ListItem>
    );
};
