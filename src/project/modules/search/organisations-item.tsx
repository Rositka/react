import React, {FC} from 'react';
import {useHistory, useRouteMatch} from 'react-router-dom';
import {Avatar, ListItem, ListItemAvatar, ListItemText} from '@material-ui/core';
import {OrganisationsSearchResult} from '../typedef';
import {makeStyles} from '@material-ui/core/styles';

interface Props {
    org: OrganisationsSearchResult
}

const useStyles = makeStyles((theme) => ({
    paper: {
        maxWidth: '400px',
        width: '100%',
        margin: '0 auto',
    },
}));

export const OrganisationsItem: FC<Props> = ({org}) => {
    const styles = useStyles();
    let {path} = useRouteMatch();
    let history = useHistory();

    const openRepo = () => {
        history.replace(`${path}/${org?.owner?.login}/`);
    }

    return (
        <ListItem className={styles.paper} button={true} onClick={openRepo}>
            <ListItemAvatar>
                <Avatar src={org.owner?.avatar_url}/>
            </ListItemAvatar>
            <ListItemText>
                <div>{org.owner?.login}</div>
                <div>{org.description}</div>
            </ListItemText>
        </ListItem>
    );
};
