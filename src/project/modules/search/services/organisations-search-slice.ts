import {createAsyncThunk, createEntityAdapter, createSlice} from '@reduxjs/toolkit';
import {OrganisationsSearchResult} from '../../typedef';
import {octokit} from '../../../octokit';
import {AppState} from '../../../store/typedef';

const organisationsAdapter = createEntityAdapter<OrganisationsSearchResult>();

export const loadOrganisations = createAsyncThunk('organisations', async (org: string) => {
    return (await octokit().repos.listForOrg({org})).data
});

export const searchOrganisations = createSlice({
    name: 'organisations',
    initialState: organisationsAdapter.getInitialState({
        loading: false,
    }),
    reducers: {
        clearDataOrg: organisationsAdapter.removeAll
    },
    extraReducers: builder => builder
        .addCase(loadOrganisations.fulfilled, (state, action) => {
            organisationsAdapter.setAll(state, action);
            state.loading = false;
        })
        .addCase(loadOrganisations.pending, state => {
            state.loading = true;
        })
        .addCase(loadOrganisations.rejected, state => {
            state.loading = false;
        })
});

export const searchOrgSelectors = organisationsAdapter.getSelectors<AppState>(state => state.orgSearch);
export const {clearDataOrg} = searchOrganisations.actions;
