import {createAsyncThunk, createEntityAdapter, createSlice} from '@reduxjs/toolkit';
import {ReposSearchResult} from '../../typedef';
import {octokit} from '../../../octokit';
import {AppState} from '../../../store/typedef';

const reposAdapter = createEntityAdapter<ReposSearchResult>();

export const loadRepos = createAsyncThunk('repos', async (q: string) => {
    return (await octokit().search.repos({q})).data.items
});

export const searchRepos = createSlice({
    name: 'repos',
    initialState: reposAdapter.getInitialState({
        loading: false,
    }),
    reducers: {
        clearData: reposAdapter.removeAll
    },
    extraReducers: builder => builder
        .addCase(loadRepos.fulfilled, (state, action) => {
            reposAdapter.setAll(state, action);
            state.loading = false;
        })
        .addCase(loadRepos.pending, state => {
            state.loading = true;
        })
        .addCase(loadRepos.rejected, state => {
            state.loading = false;
        })
});

export const searchReposSelectors = reposAdapter.getSelectors<AppState>(state => state.reposSearch);
export const {clearData} = searchRepos.actions;
