import React, {useEffect, useState} from 'react';
import {Box, Card, CardContent, CardHeader} from '@material-ui/core';
import {useDispatch, useSelector} from 'react-redux';
import {NavLink, useRouteMatch, useParams} from 'react-router-dom';
import {OwnerRepo} from '../typedef';
import {loadReposInfo, reposInfoSelectors} from './services/repos-details-slice';
import {makeOwnerRepoId} from './utils/make-owner-repo-Id';
import {makeStyles} from '@material-ui/core/styles';
import StarIcon from '@material-ui/icons/Star';
import Button from '@material-ui/core/Button';
import {octokit} from '../../octokit';
import {StarOutline} from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
    paper: {
        maxWidth: '500px',
        width: '100%',
        margin: '0 auto',
        lineHeight: '22px',
    },
    blockStar: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-end',
    },
    button: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-end',
        border: '1px solid gray',
        marginRight: '5px',
    },
}));


export const ReposDetails = () => {
    let {url} = useRouteMatch();
    const styles = useStyles();
    const params = useParams<OwnerRepo>();
    const dispatch = useDispatch();
    const reposInfo = useSelector(state => reposInfoSelectors.selectById(state, makeOwnerRepoId(params)));
    const token = useSelector(state => state.getToken.token);
    const [fork, setFork] = useState(false);
    const [star, setStar] = useState(false);

    useEffect(() => {
        dispatch(loadReposInfo(params));
        checkStar().then();
    }, [params]);

    const onCreateFork = async () => {
        try {
            const response = (await octokit().repos.createFork({
                owner: `${params.owner}`,
                repo: `${params.repo}`,
            }));
            // @ts-ignore
            if (response.status === 202) {
                setFork(true);
            } else setFork(false)
        } catch (err) {
            setFork(false);
        }
    };

    const checkStar = async () => {
        try {
            const response = (await octokit().activity.checkRepoIsStarredByAuthenticatedUser({
                owner: `${params.owner}`,
                repo: `${params.repo}`,
            }));
            if (response.status === 204) {
                setStar(true);
            } else setStar(false)
        } catch (err) {
            setStar(false);
        }
    };

    const onChangeUnStar = async () => {
        try {
            setStar(false);
            return (await octokit().activity.unstarRepoForAuthenticatedUser(params));
        } catch (err) {
            setStar(false);
        }
    };

    const onChangeStar = async () => {
        try {
            setStar(true);
            return (await octokit().activity.starRepoForAuthenticatedUser(params));
        } catch (err) {
            setStar(false);
        }
    };

    return (
        <Card className={styles.paper}>
            <CardHeader
                title={reposInfo?.name}
                subheader={reposInfo?.created_at}
            />
            <CardContent>
                {reposInfo?.description}
                <Box>License: {reposInfo?.license?.name}</Box>
                <Box>Contributors: <NavLink to={`${url}/contributors`}>{reposInfo?.contributors_url || 0}</NavLink></Box>
                <Box>Issues: <NavLink to={`${url}/issues`}>{reposInfo?.open_issues_count || 0}</NavLink></Box>
                <Box>Watchers: {reposInfo?.watchers_count}</Box>
                <Box>Forks: <NavLink to={`${url}/forks`}>{reposInfo?.forks_count || 0}</NavLink></Box>
                {token === localStorage.token
                    ?
                    <>
                        {!fork ?
                            <Button size="small" className={styles.button} onClick={onCreateFork}>Create fork</Button>
                            :
                            <div>Fork your own copy done</div>
                        }
                        <Box className={styles.blockStar}>
                            {!star ?
                                <Button color='primary' onClick={onChangeStar}>
                                    <StarOutline/>
                                    <div>Star</div>
                                    {reposInfo?.stargazers_count}
                                </Button>
                                :
                                <Button color='primary' onClick={onChangeUnStar}>
                                    <StarIcon/>
                                    <div>Unstar</div>
                                    {reposInfo?.stargazers_count}
                                </Button>
                            }
                        </Box>
                    </>
                    :
                    <div>Don't have permission, you need 'Authentication'</div>
                }
            </CardContent>
        </Card>
    )
};
