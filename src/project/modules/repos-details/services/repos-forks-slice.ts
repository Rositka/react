import {createAsyncThunk, createEntityAdapter, createSlice} from '@reduxjs/toolkit';
import {octokit} from '../../../octokit';
import {AppState} from '../../../store/typedef';

const forkAdapter = createEntityAdapter<ForkParams>();

export type ForkParams = {
    owner: string,
    repo: string
}

export const loadCreateForks = createAsyncThunk('fork/create',
    async ({owner, repo}: ForkParams) => {
        const {data} = (await octokit().repos.createFork({
            owner,
            repo,
        }));
        return data;
    });


export const createForks = createSlice({
    name: 'fork/create',
    initialState: forkAdapter.getInitialState(),
    reducers: {},
    extraReducers: builder => builder
        // @ts-ignore
        .addCase(loadCreateForks.fulfilled, forkAdapter.addOne)
});

export const createForksSelectors = forkAdapter.getSelectors<AppState>(state => state.createForksAll);
