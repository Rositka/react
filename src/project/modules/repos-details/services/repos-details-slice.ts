import {Repository} from '../../typedef';
import {createAsyncThunk, createEntityAdapter, createSlice} from '@reduxjs/toolkit';
import {makeOwnerRepoId} from '../utils/make-owner-repo-Id';
import {octokit} from '../../../octokit';
import {AppState} from '../../../store/typedef';

const reposDetailsAdapter = createEntityAdapter<Repository>({
    selectId: repo => makeOwnerRepoId({
        owner: repo.owner?.login,
        repo: repo.name
    })
});

interface Params {
    owner: string,
    repo: string
}

export const loadReposInfo = createAsyncThunk('repos-info', async ({owner, repo}: Params) => {
    return (await octokit().repos.get({owner, repo})).data;
});

export const reposDetails = createSlice({
    name: 'repos-info',
    initialState: reposDetailsAdapter.getInitialState(),
    reducers: {},
    extraReducers: builder => builder
        .addCase(loadReposInfo.fulfilled, reposDetailsAdapter.addOne)
});

export const reposInfoSelectors = reposDetailsAdapter.getSelectors<AppState>(state => state.reposDetails);
