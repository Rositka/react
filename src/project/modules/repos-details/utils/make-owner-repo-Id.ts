interface Params {
    owner?: string,
    repo: string
}

export const makeOwnerRepoId = ({owner, repo}: Params) => [owner, repo].join('/');