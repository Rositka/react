import React, {useEffect} from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import {useParams} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import {OwnerRepo} from '../typedef';
import {issuesSelectors, loadIssues} from './services/issues-slice';
import {List} from '@material-ui/core';
import {IssuePreview} from './issues-preview';

const useStyles = makeStyles((theme) => ({
    paper: {
        maxWidth: '500px',
        width: '100%',
        margin: '0 auto',
    },
}));

export const IssuesList = () => {
    const params = useParams<OwnerRepo>();
    const styles = useStyles();
    const dispatch = useDispatch();
    const issues  = useSelector(issuesSelectors.selectAll);

    useEffect(() => {
        dispatch(loadIssues(params));
    }, []);

    return (
        <div className={styles.paper}>
            <List>
                {
                    issues.map((issue, idx) => {
                        return <IssuePreview issue={issue} key={issue.id} divider={idx !== 0}/>;
                    })}
            </List>
        </div>
    )
};
