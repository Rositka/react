import {createAsyncThunk, createEntityAdapter, createSlice} from '@reduxjs/toolkit';
import {Issue} from '../../typedef';
import {octokit} from '../../../octokit';
import {AppState} from '../../../store/typedef';

interface issueParams {
    owner: string,
    repo: string,
}

const issuesAdapter = createEntityAdapter<Issue>();

export const loadIssues = createAsyncThunk('issues', async ({owner, repo}: issueParams) => {
    return (await octokit().issues.listForRepo({owner, repo})).data
});

export const issues = createSlice({
    name: 'issues',
    initialState: issuesAdapter.getInitialState({
        loading: false,
    }),
    reducers: {},
    extraReducers: builder => builder
        .addCase(loadIssues.fulfilled, (state, action) => {
            issuesAdapter.setAll(state, action);
            state.loading = false;
        })
        .addCase(loadIssues.pending, state => {
            state.loading = true;
        })
        .addCase(loadIssues.rejected, state => {
            state.loading = false;
        })
});

export const issuesSelectors = issuesAdapter.getSelectors<AppState>(state => state.issuesAll);

