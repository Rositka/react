import {createAsyncThunk, createEntityAdapter, createSlice} from '@reduxjs/toolkit';
import {Issue} from '../../typedef';
import {octokit} from '../../../octokit';
import {AppState} from '../../../store/typedef';

interface issueDetailsParams {
    owner: string,
    repo: string,
    issue_number: number
}

const issuesDetailsAdapter = createEntityAdapter<Issue>();

export const loadDetailsIssues = createAsyncThunk('issues/details', async ({owner, repo, issue_number}: issueDetailsParams) => {
    return (await octokit().issues.get({issue_number, owner, repo})).data
});

export const issuesDetails = createSlice({
    name: 'issues/details',
    initialState: issuesDetailsAdapter.getInitialState({
        loading: false,
    }),
    reducers: {},
    extraReducers: builder => builder
        .addCase(loadDetailsIssues.fulfilled, (state, action) => {
            issuesDetailsAdapter.addOne(state, action);
            state.loading = false;
        })
        .addCase(loadDetailsIssues.pending, state => {
            state.loading = true;
        })
        .addCase(loadDetailsIssues.rejected, state => {
            state.loading = false;
        })
});

export const issuesDetailsSelectors = issuesDetailsAdapter.getSelectors<AppState>(state => state.issuesAllDetails);

