import {createEntityAdapter, createSlice} from '@reduxjs/toolkit';
import {issueCloseReject, issueCloseRequest, issueCloseSuccess} from './action';
import {Issue} from '../../typedef';
import {AppState} from '../../../store/typedef';

const issuesCloseAdapter = createEntityAdapter<Issue>();

export const closeIssues = createSlice({
    name: 'issues/close',
    initialState: issuesCloseAdapter.getInitialState({
        loading: false,
    }),
    reducers: {},
    extraReducers: builder => builder
        .addCase(issueCloseSuccess, (state, action) => {
            issuesCloseAdapter.removeAll(state);
            state.loading = false;
        })
        .addCase(issueCloseRequest, state => {
            state.loading = true;
        })
        .addCase(issueCloseReject, state => {
            state.loading = false;
        })
});

export const issuesCloseSelectors = issuesCloseAdapter.getSelectors<AppState>(state => state.issuesCloseAll);
