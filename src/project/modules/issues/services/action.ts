import {createAction} from '@reduxjs/toolkit';
import {AppDispatch} from '../../../store/typedef';
import {octokit} from '../../../octokit';
import {Issue} from '../../typedef';

interface issueClose {
    owner: string,
    repo: string,
    issue_number: number
}

export const issueCloseRequest = createAction('issue/request');
export const issueCloseSuccess = createAction<Issue>('issue/success');
export const issueCloseReject = createAction('issue/reject');

export const issuesClose = ({owner, repo, issue_number}: issueClose) => async (dispatch: AppDispatch) => {
    dispatch(issueCloseRequest());

    try {
        const response = await octokit().issues.update({
            owner,
            repo,
            issue_number,
            state: 'closed'
        });
        if (response.status === 200) {
            dispatch(issueCloseSuccess(response.data));
        }
    }
    catch (error) {
        dispatch(issueCloseReject());
    }
}
