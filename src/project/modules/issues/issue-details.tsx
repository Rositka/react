import {useParams} from 'react-router-dom';
import React, {useEffect, useState} from 'react';
import {OwnerRepo} from '../typedef';
import {Avatar, Card, CardContent, CardHeader} from '@material-ui/core';
import {Comments} from './comments/comments';
import {useDispatch, useSelector} from 'react-redux';
import {loadDetailsIssues} from './services/issue-details-slice';
import {makeStyles} from "@material-ui/core/styles";
import {octokit} from "../../octokit";
import Button from "@material-ui/core/Button";

interface RouterParams extends OwnerRepo {
    id: string
}

const useStyles = makeStyles({

    paper: {
        maxWidth: '800px',
        width: '100%',
        margin: '0 auto'
    },
    button: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-end',
        border: '1px solid gray',
        marginRight: '5px',
    },
    buttonBlock: {
        display: 'flex',
        margin: '5px',
    }
});

export const IssueDetails = () => {
    const {id, owner, repo} = useParams<RouterParams>();
    const params = useParams<OwnerRepo>();
    const styles = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.getUser.user);
    const issue = useSelector(state => state.issuesAllDetails.entities[`${state.issuesAllDetails.ids[0]}`]);
    const [subscribe, setSubscribe] = useState(false);

    useEffect(() => {
        dispatch(loadDetailsIssues({issue_number: +id, owner, repo}));
        checkUserCanBeAssigned().then();
    }, []);

    const checkUserCanBeAssigned = async () => {
        try {
            const response = (await octokit().issues.checkUserCanBeAssigned({
                owner: `${params.owner}`,
                repo: `${params.repo}`,
                assignee: `${user?.login}`,
            }));
            console.log(response);
            if (response.status === 204) {
                setSubscribe(true);
            } else setSubscribe(false)
        } catch (err) {
            setSubscribe(false);
        }
    };

    const onChangeSubscribe = async () => {
        try {
            setSubscribe(true);
            return (await octokit().issues.addAssignees({
                owner: `${params.owner}`,
                repo: `${params.repo}`,
                issue_number: +id,
            }));
        } catch (err) {
            setSubscribe(false);
        }
    };

    const onChangeUnSubscribe = async () => {
        try {
            setSubscribe(false);
            return (await octokit().issues.removeAssignees({
                owner: `${params.owner}`,
                repo: `${params.repo}`,
                issue_number: +id,
            }));
        } catch (err) {
            setSubscribe(false);
        }
    };

    return (
        <Card className={styles.paper}>
            <CardHeader
                avatar={<Avatar src={issue?.user?.avatar_url} />}
                title={issue?.title}
                subheader={issue?.created_at}
            />
            <CardContent>
                {issue?.body}
                {issue && <Comments/>}
            </CardContent>
            <div className={styles.buttonBlock}>
                {!subscribe ?
                    <Button size="small" className={styles.button} onClick={onChangeSubscribe}>Subscribe</Button>
                    :
                    <Button size="small" className={styles.button} onClick={onChangeUnSubscribe}>Unsubscribe</Button>
                }
            </div>
        </Card>
    );
};
