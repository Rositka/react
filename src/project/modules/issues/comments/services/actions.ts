import {createAsyncThunk} from '@reduxjs/toolkit';
import {octokit} from '../../../../octokit';

type CommentParams = {
    issue_number: number,
    owner: string,
    repo: string
}

export const loadComments = createAsyncThunk('comments/load',
    async ({issue_number, owner, repo}: CommentParams) => {
    return (await (octokit().issues.listComments({issue_number, owner, repo}))).data;
});


type CreateCommentParams = CommentParams & {
    body: string
}

export const createComment = createAsyncThunk('comments/create',
    async ({owner, repo, issue_number, body}: CreateCommentParams) => {
    return (await octokit().issues.createComment({
            owner,
            repo,
            issue_number,
            body
    })).data;
});

type UpdateCommentParams = {
    owner: string,
    repo: string,
    comment_id: number,
    issue_number: number,
    body: string
}

export const updateComment = createAsyncThunk('comments/update',
    async (params: UpdateCommentParams) => {
        return (await octokit().issues.updateComment(params)).data;
    });


type DeleteCommentParams = {
    owner: string,
    repo: string,
    comment_id: number,
    issue_number: number
}

export const deleteComment = createAsyncThunk('comments/delete', async (params: DeleteCommentParams) => {
    return await octokit().issues.deleteComment(params);
});
