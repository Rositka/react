import {createReducer} from '@reduxjs/toolkit';
import {IssueComment} from '../../../typedef';
import {createComment, deleteComment, loadComments, updateComment} from './actions';

type State = {
    [owner: string]: {
        [repo: string]: {
            [issue_number: number]: IssueComment[]
        }
    }
};

const defaultState = {};

export const comments = createReducer<State>(defaultState, builder =>
    // @ts-ignore
    builder
        .addCase(loadComments.fulfilled, (state, action) => {
            const {owner, repo, issue_number} = action.meta.arg;

            return {
                ...state,
                [owner]: {
                    ...(state[owner] || {}),
                    [repo]: {
                        ...(state[repo]?.[owner] || {}),
                        [issue_number]: action.payload
                    }
                }
            }
        })
        .addCase(createComment.fulfilled, (state, action) => {
            const {owner, repo, issue_number} = action.meta.arg;
            // @ts-ignore
            state[owner][repo][issue_number].push(action.payload);
        })
        .addCase(updateComment.fulfilled, (state, action) => {
            const {owner, repo, issue_number, comment_id} = action.meta.arg;
            state[owner][repo][issue_number].forEach(
                comment => {
                    if (comment.id === comment_id) {
                        comment.body = action.payload.body
                    }
                    return state;
                }
            )
        })
        .addCase(deleteComment.fulfilled, (state, action) => {
            const {owner, repo, comment_id, issue_number} = action.meta.arg;
            state[owner][repo][issue_number] = state[owner][repo][issue_number].filter(
                comment => comment.id !== comment_id
            )
            return state;
        })
        .addDefaultCase(state => state)
)
