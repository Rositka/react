import React, {ChangeEvent, FC, useCallback, useState} from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {IssueComment, OwnerRepo} from '../../typedef';
import {Avatar, CardHeader, TextField} from '@material-ui/core';
import {useDispatch, useSelector} from 'react-redux';
import {makeStyles} from '@material-ui/core/styles';
import {updateComment} from './services/actions';
import {useParams} from 'react-router-dom';

interface Props {
    comment: IssueComment,
    removeComment: (id: number) => void
}

interface RouterParams extends OwnerRepo {
    id: string,
    body: string
}

const useStyles = makeStyles({
    paper: {
        minWidth: 275,
        marginTop: 25,
    },
    block: {
        maxWidth: '350px',
        width: '100%',
    },
    buttonBlock: {
        margin: '5px'
    }
});

export const CommentItem: FC<Props> = ({comment, removeComment}) => {
    const {id: idStr, owner, repo} = useParams<RouterParams>();
    const id = +idStr;
    const styles = useStyles();
    const dispatch = useDispatch();
    const token = useSelector(state => state.getToken.token);
    const [visible, setVisible] = useState(false);
    const [newBodyComment, setNewBodyComment] = useState(comment.body || '');

    const onCommentChange = useCallback((e: ChangeEvent<HTMLTextAreaElement>) => {
        setNewBodyComment(e.target.value);
    }, []);

    const onVisible = () => {
        setVisible(true);
    };

    const unVisible = () => {
        setVisible(false);
        setNewBodyComment(comment.body || '');
    };

    const onRemoveComment = () => removeComment(comment.id);

    const changeComment = () => {
        dispatch(updateComment({
            comment_id: comment.id,
            owner: owner,
            repo: repo,
            issue_number: id,
            body: newBodyComment,
        }));
        setNewBodyComment('');
    }

    return (
        <Card className={styles.paper}>
            <CardHeader
                avatar={<Avatar src={comment.user?.avatar_url}/>}
                title={comment.user?.login}
                subheader={comment.created_at}
            />
            <CardContent>
                <Typography variant="body2" color="textPrimary">
                    {comment.body}
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small">Reply</Button>
                {
                    token === localStorage.token ?
                    <>
                        <Button size="small" onClick={onRemoveComment}>Remove comment</Button>
                        <Button size="small" onClick={onVisible}>Change comment</Button>
                        {
                            (visible && token === localStorage.token) ?
                            <Card className={styles.block}>
                                <CardContent>
                                    <TextField
                                        onChange={onCommentChange}
                                        fullWidth
                                        label="Change a comment"
                                        multiline
                                        rows={4}
                                        placeholder="Change a comment"
                                        value={newBodyComment}
                                    />
                                </CardContent>
                                <Button
                                    className={styles.buttonBlock}
                                    variant="outlined"
                                    size="small"
                                    color="primary"
                                    onClick={changeComment}
                                >
                                    change
                                </Button>
                                <Button
                                    className={styles.buttonBlock}
                                    variant="outlined"
                                    size="small"
                                    color="primary"
                                    onClick={unVisible}
                                >
                                    close
                                </Button>
                            </Card>
                            :
                            null
                        }
                    </>
                    :
                    <div>Don't have permission, you need 'Authentication'</div>
                }
            </CardActions>
        </Card>
    );
}
