import React, {ChangeEvent, FC, useCallback, useState} from 'react';
import {useHistory, useParams} from 'react-router-dom';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import {TextField} from '@material-ui/core';
import {useDispatch, useSelector} from 'react-redux';
import {createComment} from './services/actions';
import {issuesClose} from '../services/action';
import {OwnerRepo} from '../../typedef';

const useStyles = makeStyles({
    paper: {
        minWidth: 275,
        marginTop: 25
    },
});

interface Props {
    issueId: number,
}

export const CommentForm: FC<Props> = ({issueId}) => {
    const styles = useStyles();
    const dispatch = useDispatch();
    const [comment, setComment] = useState('');
    const history = useHistory();
    const {owner, repo} = useParams<OwnerRepo>();
    const token = useSelector(state => state.getToken.token);

    const onCommentChange = useCallback((e: ChangeEvent<HTMLTextAreaElement>) => {
        setComment(e.target.value);
    }, []);

    const submitComment = () => {
        dispatch(createComment({
            owner: `${owner}`,
            repo: `${repo}`,
            issue_number: issueId,
            body: comment
        }));
        setComment('');
    }

    const onCloseIssue = () => {
        dispatch(issuesClose({
            owner: `${owner}`,
            repo: `${repo}`,
            issue_number: issueId,
        }));
        let path = `/project/${owner}/${repo}`;
        history.push(path);
    }

    return (
        <Card className={styles.paper}>
            <CardContent>
                <TextField
                    onChange={onCommentChange}
                    fullWidth
                    label="Leave a comment"
                    multiline
                    rows={4}
                    placeholder="Leave a comment"
                    value={comment}
                />
            </CardContent>
            <CardActions>
                <Button size="small" onClick={submitComment}>Submit</Button>
                {
                    token === localStorage.token
                        ?
                            <Button size="small" onClick={onCloseIssue}>Close issue</Button>
                        :
                            <div>Don't have permission, you need 'Authentication'</div>
                }
            </CardActions>
        </Card>
    );
}
