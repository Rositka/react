import React, {FC, useCallback} from 'react';
import {useHistory, useRouteMatch} from 'react-router-dom';
import {Issue} from '../typedef';
import {Avatar, Box, Divider, ListItem, ListItemAvatar, ListItemText} from '@material-ui/core';

interface Props {
    issue: Issue,
    divider: boolean
}

export const IssuePreview: FC<Props> = ({issue, divider}) => {
    const {url} = useRouteMatch();
    const history = useHistory();

    const openIssue = useCallback(() => {
        history.replace(`${url}/${issue.number}`);
    }, [])

    return (
        <>
            {divider && <Divider/>}
            <ListItem button={true} onClick={openIssue}>
                <ListItemAvatar>
                    <Avatar src={issue.user?.avatar_url}/>
                </ListItemAvatar>
                <ListItemText>
                    <Box>{issue.title}</Box>
                    <Box>Opened by {issue.user?.login}</Box>
                    <Box>{new Date(issue.created_at).toUTCString()}</Box>
                </ListItemText>
            </ListItem>
        </>
    );
};
