import {Octokit} from '@octokit/rest';

const octokit = () => new Octokit({
    auth: localStorage.token || '',
});

export {octokit};
