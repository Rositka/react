import {combineReducers} from 'redux';
import {searchRepos} from '../modules/search/services/repos-seach-slice';
import {searchOrganisations} from '../modules/search/services/organisations-search-slice';
import {reposDetails} from '../modules/repos-details/services/repos-details-slice';
import {organisationsDetails} from '../modules/organization-detais/services/organisations-details-slice';
import {issues} from '../modules/issues/services/issues-slice';
import {contributors} from '../modules/contributors/services/contributors-slice';
import {forks} from '../modules/forks/services/fork-slice';
import {comments} from '../modules/issues/comments/services/reducers';
import {issuesDetails} from '../modules/issues/services/issue-details-slice';
import {getToken} from '../modules/authentification/services/token/reduser';
import {getUser} from '../modules/authentification/services/user/reduser';
import {closeIssues} from '../modules/issues/services/reduser';
import {createForks} from '../modules/repos-details/services/repos-forks-slice';

export const rootReducer = combineReducers({
    comments,
    getToken,
    getUser,
    reposSearch: searchRepos.reducer,
    orgSearch: searchOrganisations.reducer,
    reposDetails: reposDetails.reducer,
    organisationsDetails: organisationsDetails.reducer,
    issuesAll: issues.reducer,
    issuesAllDetails: issuesDetails.reducer,
    issuesCloseAll: closeIssues.reducer,
    contributorsAll: contributors.reducer,
    createForksAll: createForks.reducer,
    forksAll: forks.reducer,
});
