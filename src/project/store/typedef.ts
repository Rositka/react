import {rootReducer} from './root-reducer';
import {store} from './store';

export type AppState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof store.dispatch;

declare module 'react-redux' {
    export interface DefaultRootState extends AppState {
    }
}
