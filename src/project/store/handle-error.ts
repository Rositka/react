import {Middleware} from 'redux';

export const handleError: Middleware = () => next => action => {
  if (action.error) {
      if (action.error.message === 'Bad credentials') {
          window.location.replace('/auth');
      }
  }
  next(action);
};

