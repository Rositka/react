import  {configureStore} from '@reduxjs/toolkit';
import {rootReducer} from './root-reducer';
import {handleError} from './handle-error';

export const store = configureStore({
    reducer: rootReducer,
    middleware: getDefaultMiddleware => getDefaultMiddleware().concat(handleError)
});



