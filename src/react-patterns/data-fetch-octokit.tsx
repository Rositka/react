import React from 'react';
import { CallbackParams, Data } from './typedef';
import { Loader } from './components/loader';
import { Pagination } from '@material-ui/lab';
import {withWrappedHoc} from './hocs/with-data-fetch';

const Orgs: React.FC<CallbackParams<Data>> = (
    { data, loading, page, setPage, perPage, setPerPage, totalPages }) => {
    return (
        <>
            {loading && <Loader/>}
            <Pagination
                page={page}
                onChange={(e, page) => setPage(page)}
                count={totalPages}
            />
            <select value={perPage} onChange={e => setPerPage(+e.target.value)}>
                <option value={5}>5</option>
                <option value={10}>10</option>
                <option value={20}>20</option>
                <option value={50}>50</option>
            </select>
            {data && <pre>{JSON.stringify(data, null, 4)}</pre>}
        </>
    );
};

export default withWrappedHoc(Orgs, 'https://api.github.com/orgs/facebook/repos');
