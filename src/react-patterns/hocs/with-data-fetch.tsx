import React, { ComponentType } from 'react';
import { DataFetchSection } from '../data-fetch';
import { CallbackParams } from '../typedef';

export const withWrappedHoc = <T, P extends CallbackParams<T>>(Cmp: ComponentType<P>, url: string) => {
    type InnerProps = Omit<P, keyof CallbackParams<T>>;
    const wrapped = (props: InnerProps) => {
        return (
            <DataFetchSection<T> url={url}>
                {
                    (injectedProps: CallbackParams<T>) => {
                        return <Cmp {...injectedProps as P} {...props} />;
                    }
                }
            </DataFetchSection>
        );
    };
    return wrapped;
};
