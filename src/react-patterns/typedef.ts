// @ts-ignore
import { components } from '@octokit/openapi-types/generated/types';

export type Data = components['schemas']['repository'];

export type CallbackParams<T> = {
    data: T[],
    loading: boolean,
    page: number,
    setPage: (page: number) => void,
    perPage: number,
    setPerPage: (perPage: number) => void,
    totalPages: number,
    setTotalPages?: (totalPages: number) => void
}
