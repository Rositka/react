import { ReactElement, useEffect, useState } from 'react';
import { CallbackParams } from './typedef';
import parseLink from 'parse-link-header';

interface Props<T> {
    children: (params: CallbackParams<T>) => ReactElement,
    url: string
}

export function DataFetchSection<T> (props: Props<T>) {
    const [data, setData] = useState<T[]>([]);
    const [loading, setLoading] = useState(false);
    const [page, setPage] = useState(1);
    const [perPage, setPerPage] = useState(5);
    const [totalPages, setTotalPages] = useState(0);

    useEffect(() => {
        fetchData();
    }, [page]);

    useEffect(() => {
        setPage(1);
    }, [perPage]);

    const fetchData = async () => {
        setLoading(true);
        const response = await fetch(`${props.url}?page=${page}&per_page=${perPage}`);
        const data = await response.json();
        const params = parseLink(`${response.headers.get('link' || '')}`);
        setData(data);
        if (params !== null) {
            setTotalPages(+params?.last?.page || +params?.prev?.page + 1);
            console.log(params);
        }
        setLoading(false);
    };

    return props.children({
        data,
        loading,
        page,
        setPage,
        perPage,
        setPerPage,
        totalPages
    });
}
