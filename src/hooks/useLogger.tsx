import {useEffect, useRef} from 'react';

export function useLogger<Type>(name: string, props: Type) {
    const prevValue = useRef<Type>(props);

    useEffect(() => {
        prevValue.current = props;
        type K = keyof Type;
        const keys = Object.keys(props) as K[];
        for (let key of keys) {
            console.log(`${name} changed from ${prevValue.current[key]} to ${props[key]}`);

        }
    }, [props, name]);

    return null;
}

