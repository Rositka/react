import React, {useEffect, useState} from 'react';
import {useLogger} from '../../useLogger';
import './Counter.scss';

export  function Counter() {
    const [count, setCount] = useState(0);

    useEffect(() => {
        setInterval(() => {
            setCount(count => count + 1);
        }, 2000)
    }, [])

    return (
        <>
            <p className='count-text'>Counter</p>
            <div className='count'>
                {useLogger('Counter', {count})}
            </div>
        </>
    );
}
