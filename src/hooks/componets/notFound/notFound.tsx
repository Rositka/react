import React from 'react';
import {useHistory} from 'react-router-dom';
import './notFound.scss';

export const NotFound = () => {
    const history = useHistory();
    const goBack = () => history.goBack();

    return (
        <div className='block-not-found'>
            <p className='not-found'>404 not found</p>
            <button className='button' onClick={goBack}>Go Back</button>
        </div>
    )
}
