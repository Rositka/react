export function useFilter<Type>(data: Type[], Fn: Array<(item: Type) => boolean>) {
    return (
        Fn.reduce((acom, current) => [...acom.filter(current)], data)
    )
}
